/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./src/**/*.{html,js}",
    "./about-section/**/*.html",
    "./home-section/**/*.html",
    "./projects-section/**/*.html",
    "./projects-section/**/*.js",
  ],
  theme: {
    extend: {},
  },
  plugins: [],
};
